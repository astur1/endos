﻿DROP DATABASE IF EXISTS endostablas;
CREATE DATABASE IF NOT EXISTS endostablas;
USE endostablas;
CREATE TABLE clientes(
idcli int,
  nombre varchar(40),
  PRIMARY KEY(idcli)
);
CREATE TABLE productos(
idpro int,
  nombre varchar,
  peso int,
fecha date,
  cantidad int,
  idcli int,
  PRIMARY KEY(idpro, idcli),
  UNIQUE KEY(idpro),
  CONSTRAINT fkprocli FOREIGN KEY(idcli) REFERENCES clientes(idcli)
);
